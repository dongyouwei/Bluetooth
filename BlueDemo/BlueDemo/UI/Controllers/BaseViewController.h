//
//  BaseViewController.h
//  BlueDemo
//
//  Created by wulanzhou on 16/4/18.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

/**
 *  子类化返回事件重写
 *
 *  @return YES则可以返回，NO则不能返回
 */
- (BOOL)isNavigationBack;

@end
